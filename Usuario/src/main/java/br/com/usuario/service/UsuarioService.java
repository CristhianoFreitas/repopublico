package br.com.usuario.service;

import java.util.List;

import br.com.usuario.model.Usuario;

public interface UsuarioService {

	public List<Usuario> listar();
	
	public Usuario recuperar(Long id);
	
	public void persistir(Usuario obj);
	
	public void remover(Long id);
}
