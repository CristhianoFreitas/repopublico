package br.com.usuario.service.impl;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.usuario.Application;
import br.com.usuario.dao.UsuarioRepository;
import br.com.usuario.exception.NaoEncontradoException;
import br.com.usuario.exception.ServicoBaseException;
import br.com.usuario.exception.ValidacaoException;
import br.com.usuario.model.Usuario;
import br.com.usuario.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	@Autowired
	private UsuarioRepository repo;

	@Override
	public List<Usuario> listar() {
		log.debug("Inicio");

		final List<Usuario> lista = StreamSupport.stream(repo.findAll().spliterator(), false)
				.collect(Collectors.toList());

		if (lista.isEmpty()) {
			throw new ValidacaoException("Lista de usuários vazia");
		}
		return lista;
	}

	@Override
	public Usuario recuperar(final Long id) {
		log.debug("Inicio, {}", id);

		return repo.findById(id).map(Function.identity()).orElseThrow(() -> new NaoEncontradoException("Não Encontrado um usuário"));
	}

	@Override
	public void persistir(final Usuario obj) {
		log.debug("Inicio, {}", obj);
		try {
			
			if (obj.getNome() == null) {
				throw new Exception("Erro nao esperado");
			}
			repo.save(obj);

		} catch (final Exception e) {
			e.printStackTrace();
			throw new ServicoBaseException(e.getMessage());
		}
	}

	@Override
	public void remover(final Long id) {
		log.debug("Inicio, {}", id);

		repo.deleteById(id);
	}

}
