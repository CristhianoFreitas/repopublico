package br.com.usuario.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/")
@RestController
public class InicialController {

	
	@GetMapping("/")
	public String home() {
		return "Tela Inicial";
	}
}
