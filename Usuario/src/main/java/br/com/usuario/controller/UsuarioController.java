package br.com.usuario.controller;

import static br.com.usuario.controller.ResponseAplicacao.SUCESSO;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.usuario.model.Usuario;
import br.com.usuario.service.UsuarioService;

@RequestMapping("/usuario")
@RestController
public class UsuarioController {

	@Autowired
	private UsuarioService service;
	
	
	@GetMapping()
	public ResponseAplicacao<?> listar() {
		
		final List<Usuario> conteudo = service.listar();
		return new ResponseAplicacao<>(HttpStatus.OK, SUCESSO, conteudo);
	}
	
	@GetMapping("/{id}")
	public ResponseAplicacao<?> recuperar(@PathVariable("id") final Long id) {
		
		final Usuario conteudo = service.recuperar(id);
		return new ResponseAplicacao<>(HttpStatus.OK, SUCESSO, conteudo);
	}
	
	@PostMapping
	public  ResponseAplicacao<?> persistir(@RequestBody final Usuario obj) {
		
		service.persistir(obj);
		return new ResponseAplicacao<>(HttpStatus.CREATED, SUCESSO, null);
	}
	
	@DeleteMapping("/{id}")
	public  ResponseAplicacao<?> remover(@PathVariable("id") final Long id) {
		
		service.remover(id);
		return new ResponseAplicacao<>(HttpStatus.OK, SUCESSO, null);
	}
}
