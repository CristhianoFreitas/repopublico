package br.com.usuario.controller;

import java.beans.Transient;
import java.io.Serializable;

import org.springframework.http.HttpStatus;

public class ResponseAplicacao<T> implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String SUCESSO = "Sucesso";

	private int codigo;
	private String mensagem;
	private T conteudo;
	
	public ResponseAplicacao(final int codigo, final String mensagem, final T conteudo) {
		super();
		this.codigo = codigo;
		this.mensagem = mensagem;
		this.conteudo = conteudo;
	}
	
	public ResponseAplicacao(final HttpStatus hs, final String mensagem, final T conteudo) {
		super();
		setCodigo(hs);
		this.mensagem = mensagem;
		this.conteudo = conteudo;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(final int codigo) {
		this.codigo = codigo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(final String mensagem) {
		this.mensagem = mensagem;
	}

	public T getConteudo() {
		return conteudo;
	}

	public void setConteudo(final T conteudo) {
		this.conteudo = conteudo;
	}

	@Transient
	public HttpStatus getHttpStatus() {
		return HttpStatus.valueOf(codigo);
	}

	public void setCodigo(final HttpStatus hs) {
		codigo = hs.value();
	}

	@Override
	public String toString() {
		return String.format("ResponseCustom [codigo=%s, mensagem=%s, conteudo=%s]", codigo, mensagem, conteudo);
	}
}
