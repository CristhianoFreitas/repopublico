package br.com.usuario.exception;


/**
 * Classe Base para as excecoes do sistema
 * 
 * @author Cris
 *
 */
public class ServicoBaseException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public ServicoBaseException(final String mensagem) {
		super(mensagem);
	}


}
