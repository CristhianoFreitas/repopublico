package br.com.usuario.exception;

public class ValidacaoException extends ServicoBaseException {

	private static final long serialVersionUID = 1L;

	public ValidacaoException(final String mensagem) {
		super(mensagem);
	}


}
