package br.com.usuario.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import br.com.usuario.controller.ResponseAplicacao;
import br.com.usuario.exception.NaoEncontradoException;
import br.com.usuario.exception.ValidacaoException;

@RestControllerAdvice
public class AplicacaoExceptionHandler {
	
	///*
	// Manipulacao usando o Response Entity padrao
	
	@ExceptionHandler(value = Exception.class)
	public ResponseEntity<ResponseAplicacao<?>> exception(final Exception exception, final WebRequest request) {
		final ResponseAplicacao response = new ResponseAplicacao<>(HttpStatus.INTERNAL_SERVER_ERROR, "Erro na aplicação", exception.getMessage());
		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(value = NaoEncontradoException.class)
	public ResponseEntity<ResponseAplicacao<?>> exception(final NaoEncontradoException exception, final WebRequest request) {
		final ResponseAplicacao response = new ResponseAplicacao<>(HttpStatus.NOT_FOUND, "Erro ao buscar a entidade", exception.getMessage());
		return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(value = ValidacaoException.class)
	public ResponseEntity<ResponseAplicacao<?>> exception(final ValidacaoException exception, final WebRequest request) {
		final ResponseAplicacao response =  new ResponseAplicacao<>(HttpStatus.BAD_REQUEST, "Erro no camada de serviço", exception.getMessage());
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}
	
	//*/
	
	/*
	// Manipulacao usando o Response Entity padrao
	
	@ExceptionHandler(value = ServicoBaseException.class)
	public ResponseEntity<Object> exception(final ServicoBaseException exception, final WebRequest request) {
		return new ResponseEntity<>("Erro na aplicação", HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(value = NaoEncontradoException.class)
	public ResponseEntity<Object> exception(final NaoEncontradoException exception, final WebRequest request) {
		return new ResponseEntity<>("Erro ao buscar a entidade", HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value = ValidacaoException.class)
	public ResponseEntity<Object> exception(final ValidacaoException exception, final WebRequest request) {
		return new ResponseEntity<>("Erro no camada de serviço", HttpStatus.BAD_REQUEST);
	}
	
	*/
}
