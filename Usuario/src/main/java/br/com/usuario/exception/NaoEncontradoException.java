package br.com.usuario.exception;

public class NaoEncontradoException extends ServicoBaseException {

	private static final long serialVersionUID = 1L;
	
	public NaoEncontradoException(final String mensagem) {
		super(mensagem);
	}

}
