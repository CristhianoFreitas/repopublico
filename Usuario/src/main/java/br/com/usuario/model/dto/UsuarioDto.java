package br.com.usuario.model.dto;

public class UsuarioDto {

	
	private String login;
	private String senha;
	
	
	public String getLogin() {
		return login;
	}
	public void setLogin(final String login) {
		this.login = login;
	}
	
	public String getSenha() {
		return senha;
	}
	public void setSenha(final String senha) {
		this.senha = senha;
	}
	
}
