package br.com.usuario.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Contato {
	
	
	@GeneratedValue
	@Id
	private Long id;
	
	private String email;
	
	private String telefone;
	
	private String celular;
	
	
	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(final String telefone) {
		this.telefone = telefone;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(final String celular) {
		this.celular = celular;
	}

	@Override
	public String toString() {
		return String.format("Contato [id=%s, email=%s, telefone=%s, celular=%s]", id, email, telefone, celular);
	}
}
