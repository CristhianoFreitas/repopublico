package br.com.usuario.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Usuario {

	
	@GeneratedValue
	@Id
	private Long id;

	@OneToOne(cascade = {CascadeType.ALL})
	private Contato contato;
	
	private String nome;
	
	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}
	
	public Contato getContato() {
		return contato;
	}

	public void setContato(final Contato contato) {
		this.contato = contato;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return String.format("Usuario [id=%s, contato=%s, nome=%s]", id, contato, nome);
	}
}
